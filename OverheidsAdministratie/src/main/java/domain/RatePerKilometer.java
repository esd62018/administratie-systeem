package domain;

import domain.enums.RateCategory;

import javax.persistence.*;
import java.util.Date;

/**
 * RatePerKilometer entity
 *
 * Has some properties that need to be saved to know about rates.
 */
@Entity
@Table(name = "RATE_PER_KILOMETER")
@NamedQueries({
  @NamedQuery(name = "RatePerKilometer.findAll", query = "SELECT r FROM RatePerKilometer r"),
})
public class RatePerKilometer {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String region;

  @Column(name = "RATE_START")
  private String startDate;

  @Column(name = "RATE_END")
  private String endDate;

  @Enumerated(EnumType.STRING)
  @Column(name = "RATE_CATEGORY")
  private RateCategory rateCategory;

  @Column(name = "RATE_PRICE")
  private double ratePrice;

  public RatePerKilometer() {}

  public RatePerKilometer(String region, String startDate, String endDate, RateCategory rateCategory, double ratePrice) {
    this.region = region;
    this.startDate = startDate;
    this.endDate = endDate;
    this.rateCategory = rateCategory;
    this.ratePrice = ratePrice;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public RateCategory getRateCategory() {
    return rateCategory;
  }

  public void setRateCategory(RateCategory rateCategory) {
    this.rateCategory = rateCategory;
  }

  public double getRatePrice() { return ratePrice; }

  public void setRatePrice(double ratePrice) { this.ratePrice = ratePrice; }

  @Override
  public String toString() {
    return "Region: " + region + ", category: " + rateCategory + ", startDate: " + startDate + ", endDate: " + endDate + ", price: " + String.valueOf(ratePrice);
  }
}

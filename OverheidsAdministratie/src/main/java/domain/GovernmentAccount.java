package domain;

import domain.enums.Role;

import javax.persistence.*;

/**
 * GovernmentAccount entity containing login credentials
 *
 * A governmentAccount entity can have a role to access government system functionality.
 */
@Entity
@Table(name = "GOVERNMENT_ACCOUNT")
@NamedQueries({
  @NamedQuery(
    name = "GovernmentAccount.auth",
    query = "SELECT u FROM GovernmentAccount u WHERE u.username = :username AND u.password = :password"
  ),
  @NamedQuery(
    name = "GovernmentAccount.getAll",
    query = "SELECT u FROM GovernmentAccount u"
  )
})
public class GovernmentAccount {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Long id;
  @Column(unique = true, name = "USERNAME")
  private String username;
  @Column(name = "PASSWORD")
  private String password;
  @Enumerated(EnumType.STRING)
  private Role role;

  public GovernmentAccount() {

  }

  public GovernmentAccount(String username, String password) {
    this.username = username;
    this.password = password;
  }

  @PrePersist
  private void preInsert() {
    role = Role.LIMITED;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }
}

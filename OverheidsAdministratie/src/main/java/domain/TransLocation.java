package domain;

import java.util.Date;
public class TransLocation implements Comparable<TransLocation> {
    private Long id;
    private Double lat;
    private Double lon;
    private Double previousLat;
    private Double previousLon;
    private Date dateTime;
    private String serialNumber;
    private String countryCode;
    private double price;
    private double distance;

    public TransLocation() {
    }

    public TransLocation(Double lat, Double lon, Date dateTime, String serialNumber, String countryCode) {
        this.lat = lat;
        this.lon = lon;
        this.dateTime = dateTime;
        this.serialNumber = serialNumber;
        this.countryCode = countryCode;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public Double getPreviousLat() {
    return previousLat;
  }

    public Double getPreviousLon() {
    return previousLon;
  }

    public Date getDateTime() {
        return dateTime;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public double getPrice() { return price; }

    public double getDistance() { return distance; }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public void setPreviousLat(Double previousLat) {
      this.previousLat = previousLat;
    }

    public void setPreviousLon(Double previousLon) {
      this.previousLon = previousLon;
    }

    public void setDateTime(Date dateTime) {
          this.dateTime = dateTime;
      }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public void setPrice(double price) { this.price = price; }

    public void setDistance(double distance) { this.distance = distance; }

  @Override
  public int compareTo(TransLocation o) {
    return getDateTime().compareTo(o.getDateTime());
  }
}

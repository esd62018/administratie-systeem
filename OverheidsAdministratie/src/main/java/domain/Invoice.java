package domain;

import domain.enums.PaymentStatus;

import javax.persistence.*;

/**
 * Invoice entity
 *
 * Has some properties that need to be saved to keep track of payments.
 */
@Entity
@Table(name = "INVOICE")
@NamedQueries({
  @NamedQuery(name = "Invoice.findAll", query = "SELECT i FROM Invoice i"),
  @NamedQuery(name = "Invoice.findFromOwner", query = "SELECT i FROM Invoice i WHERE i.ownerId = :ownerId"),
})
public class Invoice {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "CAR_TRACKER")
  private String carTracker;

  @Column(name = "TOTAL_AMOUNT")
  private double totalAmount;

  @Column(name = "INVOICE_DATE")
  private String invoiceDate;

  @Column(name = "PAYMENT_STATUS")
  @Enumerated(EnumType.STRING)
  private PaymentStatus paymentStatus;

  @Column(name = "OWNER_ID")
  private long ownerId;

  @Column(name = "CATEGORY")
  private String category;

  public Invoice() {}

  public Invoice(long id, String carTracker, double totalAmount, String invoiceDate, PaymentStatus paymentStatus, long ownerId, String category) {
    this.id = id;
    this.carTracker = carTracker;
    this.totalAmount = totalAmount;
    this.invoiceDate = invoiceDate;
    this.paymentStatus = paymentStatus;
    this.ownerId = ownerId;
    this.category = category;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCarTracker() {
    return carTracker;
  }

  public void setCarTracker(String carTracker) {
    this.carTracker = carTracker;
  }

  public double getTotalAmount() {
    return totalAmount;
  }

  public void setTotalAmount(double totalAmount) {
    this.totalAmount = totalAmount;
  }

  public String getInvoiceDate() {
    return invoiceDate;
  }

  public void setInvoiceDate(String invoiceDate) {
    this.invoiceDate = invoiceDate;
  }

  public PaymentStatus getPaymentStatus() {
    return paymentStatus;
  }

  public void setPaymentStatus(PaymentStatus paymentStatus) {
    this.paymentStatus = paymentStatus;
  }

  public long getOwnerId() {
    return ownerId;
  }

  public void setOwnerId(long ownerId) {
    this.ownerId = ownerId;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }
}


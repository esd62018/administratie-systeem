package domain.enums;

public enum PaymentStatus {
  OPEN,
  PAID,
  CANCELLED
}

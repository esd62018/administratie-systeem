package domain.enums;

public enum RateCategory {
  ELECTRICAL,
  DIESEL,
  GASOLINE
}

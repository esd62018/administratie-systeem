package domain;

import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Journey {
    private Long id;
    private String serialNumber;
    private Date endTime;
    private List<TransLocation> transLocationList;

    public Journey() {
        this.transLocationList = new ArrayList<TransLocation>();
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public List<TransLocation> getTransLocations() {
        return this.transLocationList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public List<TransLocation> getTransLocationList() {
        return transLocationList;
    }

    public void setTransLocationList(List<TransLocation> transLocationList) {
        this.transLocationList = transLocationList;
    }

    public void addTranslocation(TransLocation transLocation){
        this.transLocationList.add(transLocation);

    }

    public Date getLastJourneyTime(){
        Date currentLastDate = null;
        try {
            currentLastDate = new SimpleDateFormat( "yyyyMMdd" ).parse( "20100520" );
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(this.transLocationList.size() > 0) {
            for (TransLocation t : this.transLocationList) {
                if (t.getDateTime() != null) {
                    if (t.getDateTime().after(currentLastDate)) {
                        currentLastDate = t.getDateTime();
                    }
                }
            }
        }
        else{
            currentLastDate.setHours(currentLastDate.getHours()-2);
        }

        return currentLastDate;
    }
}

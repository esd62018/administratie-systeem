package dto;

public class OwnerDto {
  private Long id;
  private String citizenServiceNumber;
  private String street;
  private String houseNumber;
  private String zipCode;
  private String city;
  private String country;
  private String firstName;
  private String lastName;

  public OwnerDto() {

  }

  public OwnerDto(Long id, String citizenServiceNumber, String street, String houseNumber, String zipCode, String city, String country, String firstName, String lastName) {
    this.id = id;
    this.citizenServiceNumber = citizenServiceNumber;
    this.street = street;
    this.houseNumber = houseNumber;
    this.zipCode = zipCode;
    this.city = city;
    this.country = country;
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getCitizenServiceNumber() {
    return citizenServiceNumber;
  }

  public void setCitizenServiceNumber(String citizenServiceNumber) {
    this.citizenServiceNumber = citizenServiceNumber;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String address) {
    this.street = address;
  }

  public String getHouseNumber() {
    return houseNumber;
  }

  public void setHouseNumber(String houseNumber) {
    this.houseNumber = houseNumber;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
}

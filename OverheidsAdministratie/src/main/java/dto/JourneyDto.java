package dto;

import domain.TransLocation;

import java.util.ArrayList;
import java.util.List;

public class JourneyDto{
  private String id;
  private String serialNumber;
  private String endDate;
  private List<TransLocation> transLocations;

  public JourneyDto() {
  }

  public JourneyDto(String id, String serialNumber, String endDate) {
    this.id = id;
    this.serialNumber = serialNumber;
    this.endDate = endDate;
    this.transLocations = new ArrayList<TransLocation>();
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public String getSerialNumber() {
    return serialNumber;
  }

  public void setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
  }

  public List<TransLocation> getTransLocations() {
    return transLocations;
  }

  public void setTransLocations(List<TransLocation> transLocations) {
    this.transLocations = transLocations;
  }
}

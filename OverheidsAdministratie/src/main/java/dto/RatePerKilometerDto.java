package dto;

import domain.RatePerKilometer;
import domain.enums.RateCategory;

public class RatePerKilometerDto {
  private Long id;
  private String region;
  private String startDate;
  private String endDate;
  private RateCategory rateCategory;
  private double ratePrice;

  public RatePerKilometerDto(Long id, String region, String startDate, String endDate, RateCategory rateCategory, double ratePrice) {
    this.id = id;
    this.region = region;
    this.startDate = startDate;
    this.endDate = endDate;
    this.rateCategory = rateCategory;
    this.ratePrice = ratePrice;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public RateCategory getRateCategory() {
    return rateCategory;
  }

  public void setRateCategory(RateCategory rateCategory) {
    this.rateCategory = rateCategory;
  }

  public double getRatePrice() { return ratePrice; }

  public void setRatePrice(double ratePrice) {
    this.ratePrice = ratePrice;
  }

  public static RatePerKilometerDto fromRatePerKilomter(RatePerKilometer ratePerKilometer) {
    return new RatePerKilometerDto(
      ratePerKilometer.getId(),
      ratePerKilometer.getRegion(),
      ratePerKilometer.getStartDate(),
      ratePerKilometer.getEndDate(),
      ratePerKilometer.getRateCategory(),
      ratePerKilometer.getRatePrice()
    );
  }
}

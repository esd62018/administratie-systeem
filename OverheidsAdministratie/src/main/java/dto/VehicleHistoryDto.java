package dto;

public class VehicleHistoryDto {
  private long id;
  private String fromDate;
  private String tillDate;
  private VehicleDto vehicle;
  private OwnerDto owner;

  public VehicleHistoryDto() {

  }

  public VehicleHistoryDto(long id, String fromDate, String tillDate, VehicleDto vehicle, OwnerDto owner) {
    this.id = id;
    this.fromDate = fromDate;
    this.tillDate = tillDate;
    this.vehicle = vehicle;
    this.owner = owner;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getFromDate() {
    return fromDate;
  }

  public void setFromDate(String fromDate) {
    this.fromDate = fromDate;
  }

  public String getTillDate() {
    return tillDate;
  }

  public void setTillDate(String tillDate) {
    this.tillDate = tillDate;
  }

  public VehicleDto getVehicle() {
    return vehicle;
  }

  public void setVehicle(VehicleDto vehicle) {
    this.vehicle = vehicle;
  }

  public OwnerDto getOwner() {
    return owner;
  }

  public void setOwner(OwnerDto owner) {
    this.owner = owner;
  }
}

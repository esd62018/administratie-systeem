package dto;

import domain.GovernmentAccount;

public class GovernmentAccountDto {
  private Long id;
  private String username;
  private String password;
  private String role;

  public GovernmentAccountDto () {

  }

  public GovernmentAccountDto(Long id, String username, String password, String role) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.role = role;
  }

  public GovernmentAccountDto(Long id, String username, String role) {
    this.id = id;
    this.username = username;
    this.role = role;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public static GovernmentAccountDto fromGovernmentAccount(GovernmentAccount governmentAccount) {
    return new GovernmentAccountDto(
      governmentAccount.getId(),
      governmentAccount.getUsername(),
      governmentAccount.getPassword(),
      governmentAccount.getRole().toString()
    );
  }

  public static GovernmentAccountDto fromGovernmentAccountWithoutPassword(GovernmentAccount governmentAccount) {
    return new GovernmentAccountDto(
      governmentAccount.getId(),
      governmentAccount.getUsername(),
      governmentAccount.getRole().toString()
    );
  }
}

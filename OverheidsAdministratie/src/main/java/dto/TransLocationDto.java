package dto;

import domain.TransLocation;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class TransLocationDto{
  private String serialNumber;
  private String lat;
  private String lon;
  private String dateTime;
  private String countryCode; //Country the car is driving in

  public TransLocationDto(TransLocation transLocation) {
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
    if(transLocation.getDateTime() != null) {
      String datetime = df.format(transLocation.getDateTime());
      this.dateTime = datetime;
    }
    this.serialNumber = transLocation.getSerialNumber();
    this.lat = transLocation.getLat().toString();
    this.lon = transLocation.getLon().toString();
    this.countryCode = transLocation.getCountryCode();
  }

  public TransLocationDto(String serialNumber, String lat, String lon, String dateTime, String countryCode) {
    this.serialNumber = serialNumber;
    this.lat = lat;
    this.lon = lon;
    this.dateTime = dateTime;
    this.countryCode = countryCode;
  }

  public String getSerialNumber() {
    return serialNumber;
  }

  public String getLat() {
    return lat;
  }

  public String getLon() {
    return lon;
  }

  public String getDateTime() {
    return dateTime;
  }

  public String getCountryCode() {
    return countryCode;
  }
}

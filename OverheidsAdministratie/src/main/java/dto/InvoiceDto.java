package dto;

import domain.Invoice;
import domain.enums.PaymentStatus;

import java.util.List;

public class InvoiceDto {
  private Long id;
  private String carTracker;
  private double totalAmount;
  private String invoiceDate;
  private PaymentStatus paymentStatus;
  private long ownerId;
  private List<JourneyDto> journeys;
  private String category;

  public InvoiceDto(Long id, String carTracker, double totalAmount, String invoiceDate, PaymentStatus paymentStatus, long ownerId, String category) {
    this.id = id;
    this.carTracker = carTracker;
    this.totalAmount = totalAmount;
    this.invoiceDate = invoiceDate;
    this.paymentStatus = paymentStatus;
    this.ownerId = ownerId;
    this.category = category;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCarTracker() {
    return carTracker;
  }

  public void setCarTracker(String carTracker) {
    this.carTracker = carTracker;
  }

  public double getTotalAmount() {
    return totalAmount;
  }

  public void setTotalAmount(double totalAmount) {
    this.totalAmount = totalAmount;
  }

  public String getInvoiceDate() {
    return invoiceDate;
  }

  public void setInvoiceDate(String invoiceDate) {
    this.invoiceDate = invoiceDate;
  }

  public PaymentStatus getPaymentStatus() {
    return paymentStatus;
  }

  public String getCategory() {
    return category;
  }

  public void setPaymentStatus(PaymentStatus paymentStatus) {
    this.paymentStatus = paymentStatus;
  }

  public long getOwnerId() {
    return ownerId;
  }

  public void setOwnerId(long ownerId) {
    this.ownerId = ownerId;
  }

  public List<JourneyDto> getJourneys() {
    return journeys;
  }

  public void setJourneys(List<JourneyDto> journeys) {
    this.journeys = journeys;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public static InvoiceDto fromInvoice(Invoice invoice) {
    return new InvoiceDto(
      invoice.getId(),
      invoice.getCarTracker(),
      invoice.getTotalAmount(),
      invoice.getInvoiceDate(),
      invoice.getPaymentStatus(),
      invoice.getOwnerId(),
      invoice.getCategory()
    );
  }
}

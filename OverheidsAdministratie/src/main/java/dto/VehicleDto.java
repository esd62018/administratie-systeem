package dto;

public class VehicleDto {
  private Long id;
  private String brand;
  private String model;
  private String licensePlate;
  private String buildYear;
  private String color;
  private String carTracker;

  public VehicleDto() {

  }

  public VehicleDto(Long id, String brand, String model, String licensePlate, String buildYear, String color, String carTracker) {
    this.id = id;
    this.brand = brand;
    this.model = model;
    this.licensePlate = licensePlate;
    this.buildYear = buildYear;
    this.color = color;
    this.carTracker = carTracker;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getLicensePlate() {
    return licensePlate;
  }

  public void setLicensePlate(String licensePlate) {
    this.licensePlate = licensePlate;
  }

  public String getBuildYear() {
    return buildYear;
  }

  public void setBuildYear(String buildYear) {
    this.buildYear = buildYear;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getCarTracker() {
    return carTracker;
  }

  public void setCarTracker(String carTracker) {
    this.carTracker = carTracker;
  }
}

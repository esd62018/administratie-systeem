package dao;

import domain.GovernmentAccount;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class GovernmentAccountDao {

  @PersistenceContext
  private EntityManager entityManager;

  public GovernmentAccountDao () {

  }

  public List<GovernmentAccount> getAll() {
    return entityManager
      .createNamedQuery("GovernmentAccount.getAll", GovernmentAccount.class)
      .getResultList();
  }

  public GovernmentAccount findAccountByLoginCredential(String username, String password) {
    return entityManager
      .createNamedQuery("GovernmentAccount.auth", GovernmentAccount.class)
      .setParameter("username", username)
      .setParameter("password", password)

      .getSingleResult();
  }

  public GovernmentAccount create(GovernmentAccount account) {
    entityManager.persist(account);

    return account;
  }

  public GovernmentAccount update(GovernmentAccount account) {
    entityManager.merge(account);

    return account;
  }

  public void delete(Long id) {
    entityManager.remove(entityManager.find(GovernmentAccount.class, id));
  }
}

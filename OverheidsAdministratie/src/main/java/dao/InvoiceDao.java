package dao;

import domain.Invoice;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class InvoiceDao {
  @PersistenceContext
  private EntityManager entityManager;

  public InvoiceDao() { }

  public List<Invoice> getInvoices() {
    return entityManager
      .createNamedQuery("Invoice.findAll", Invoice.class)
      .getResultList();
  }

  public List<Invoice> getInvoicesFromOwner(Long ownerId) {
    return entityManager
      .createNamedQuery("Invoice.findFromOwner", Invoice.class)
      .setParameter("ownerId", ownerId)
      .getResultList();
  }

  public Invoice findById(long id) {
    return entityManager.find(Invoice.class, id);
  }

  public Invoice create(Invoice invoice) {
    entityManager.persist(invoice);
    return invoice;
  }

  public Invoice update(Invoice invoice) {
    entityManager.merge(invoice);
    return invoice;
  }

  public void delete(long id) {
    entityManager.remove(findById(id));
  }
}

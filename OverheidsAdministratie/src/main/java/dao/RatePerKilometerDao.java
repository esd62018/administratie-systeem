package dao;

import domain.Invoice;
import domain.RatePerKilometer;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class RatePerKilometerDao {
  @PersistenceContext
  private EntityManager entityManager;

  public RatePerKilometerDao() { }

  public List<RatePerKilometer> getRatesPerKilometer() {
    return entityManager
      .createNamedQuery("RatePerKilometer.findAll", RatePerKilometer.class)
      .getResultList();
  }

  public RatePerKilometer findById(long id) {
    return entityManager.find(RatePerKilometer.class, id);
  }

  public RatePerKilometer create(RatePerKilometer ratePerKilometer) {
    entityManager.persist(ratePerKilometer);
    return ratePerKilometer;
  }

  public RatePerKilometer update(RatePerKilometer ratePerKilometer) {
    entityManager.merge(ratePerKilometer);
    return ratePerKilometer;
  }

  public void delete(long id) {
    entityManager.remove(findById(id));
  }
}

package service;

import dao.GovernmentAccountDao;
import domain.GovernmentAccount;
import domain.enums.Role;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class GovernmentAccountService {

  @Inject
  private GovernmentAccountDao governmentAccountDao;

  public GovernmentAccountService() {

  }

  public List<GovernmentAccount> getAll() {
    return governmentAccountDao.getAll();
  }

  public GovernmentAccount authenticate(GovernmentAccount governmentAccount) {
    return governmentAccountDao.findAccountByLoginCredential(governmentAccount.getUsername(), governmentAccount.getPassword());
  }

  public GovernmentAccount signUp(GovernmentAccount governmentAccount) {
    return governmentAccountDao.create(governmentAccount);
  }

  public GovernmentAccount update(GovernmentAccount governmentAccount) {
    return governmentAccountDao.update(governmentAccount);
  }

  public void delete(Long id) {
    governmentAccountDao.delete(id);
  }
}

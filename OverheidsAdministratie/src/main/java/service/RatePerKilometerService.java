package service;

import dao.RatePerKilometerDao;
import domain.RatePerKilometer;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class RatePerKilometerService {
  @Inject
  private RatePerKilometerDao ratePerKilometerDao;

  public RatePerKilometerService() {}

  public List<RatePerKilometer> findAll() {
    return ratePerKilometerDao.getRatesPerKilometer();
  }

  public RatePerKilometer findById(long id) {
    return ratePerKilometerDao.findById(id);
  }

  public RatePerKilometer create(RatePerKilometer ratePerKilometer) {
    return ratePerKilometerDao.create(ratePerKilometer);
  }

  public RatePerKilometer update(RatePerKilometer ratePerKilometer) {
    return ratePerKilometerDao.update(ratePerKilometer);
  }

  public void delete(Long id) {
    ratePerKilometerDao.delete(id);
  }
}

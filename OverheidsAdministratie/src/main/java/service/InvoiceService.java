package service;

import dao.InvoiceDao;
import domain.Invoice;
import jms.MessageProducer;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class InvoiceService {
  @Inject
  private InvoiceDao invoiceDao;

  public InvoiceService() {}

  public List<Invoice> getInvoices() {
    return invoiceDao.getInvoices();
  }

  public List<Invoice> getInvoicesFromOwner(Long ownerId) {
    return invoiceDao.getInvoicesFromOwner(ownerId);
  }

  public Invoice findById(long id) {
    return invoiceDao.findById(id);
  }

  public Invoice create(Invoice invoice) {
    return invoiceDao.create(invoice);
  }

  public Invoice update(Invoice invoice) {
    return invoiceDao.update(invoice);
  }

  public void delete(Long id) {
    invoiceDao.delete(id);
  }
}

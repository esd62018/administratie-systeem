package jms;

import dto.InvoiceDto;
import dto.TransLocationDto;
import org.json.JSONObject;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

@Startup
@Singleton
public class MessageProducer {
  private static Gateway gatewayIT;
  private static Gateway gatewayDE;
  private static Gateway gatewayFI;
  private static Gateway gatewayNL;

  private static final String ipIT = "192.168.24.170";
  private static final String ipDE = "192.168.24.99";
  private static final String ipFI = "192.168.24.100";
  private static final String ipNL = "192.168.24.61";
  private static final String channelNameDefault = "rekeningrijden.invoices";


  public MessageProducer() {

  }

  public static void sendNewInvoice(InvoiceDto invoiceDto, String originCountry) {
    JSONObject jsonObject = new JSONObject(invoiceDto);
    try {
      Gateway gateway = getGateway(originCountry);
      if(gateway != null){
        gateway.channel.basicPublish("", channelNameDefault, null, convertPayLoadToBytes(jsonObject.toString()));
      }
      System.out.println("Message send: " + jsonObject.toString());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static Gateway getGateway(String originCountry) {
    if(originCountry.equals("DE")){
      return gatewayDE;
    } else if(originCountry.equals("FI")){
      return gatewayFI;
    } else if(originCountry.equals("IT")){
      return gatewayIT;
    } else if(originCountry.equals("NL")){
      return gatewayNL;
    } else{
      return null;
    }
  }

  private static byte[] convertPayLoadToBytes(String payload) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ObjectOutputStream writter = new ObjectOutputStream(baos);
    writter.writeObject(payload);
    return baos.toByteArray();
  }

  private static void startChannels() {
    try {
      gatewayDE = new Gateway(ipDE);
      gatewayIT = new Gateway(ipIT);
      gatewayFI = new Gateway(ipFI);
      gatewayNL = new Gateway(ipNL);


      gatewayDE.channel.queueDeclare(channelNameDefault, false, false, false, null);
      gatewayIT.channel.queueDeclare(channelNameDefault, false, false, false, null);
      gatewayFI.channel.queueDeclare(channelNameDefault, false, false, false, null);
      gatewayNL.channel.queueDeclare(channelNameDefault, false, false, false, null);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  @PostConstruct
  public static void init() {
    startChannels();
  }
}

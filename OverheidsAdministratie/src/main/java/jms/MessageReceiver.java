package jms;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import dto.TransLocationDto;
import org.json.JSONObject;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

@Startup
@Singleton
public class MessageReceiver {
  private Gateway receiveEurope;
  private static final String QUEUE_NAME = "rekeningrijden.invoices";
  private static List<String> trackSerialIds;


  public MessageReceiver() {

  }

  @PostConstruct
  public void init() {
    try {
      receiveEurope = new Gateway("localhost");
      receiveEurope.channel.queueDeclare(QUEUE_NAME, true, false, false, null);
      setupReceivehandler();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void setupReceivehandler() {
    Consumer consumer = new DefaultConsumer(receiveEurope.channel) {
      @Override
      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
        throws IOException {
        System.out.println("Got something..");
        JSONObject jsonObj = new JSONObject(new String(body));
      }
    };
    try {
      receiveEurope.channel.basicConsume(QUEUE_NAME, true, consumer);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}

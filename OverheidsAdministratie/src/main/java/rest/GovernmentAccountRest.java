package rest;

import domain.GovernmentAccount;
import dto.GovernmentAccountDto;
import service.GovernmentAccountService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import java.util.List;
import java.util.stream.Collectors;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/accounts")
public class GovernmentAccountRest {
  @Inject
  private GovernmentAccountService governmentAccountService;

  /**
   * Route to get all government accounts.
   *
   * @return  a response containing a list of government accounts on success
   */
  @GET
  @Produces({ APPLICATION_JSON })
  public Response getAccounts() {
    List<GovernmentAccountDto> accounts = null;
    int status = 200;

    try {
      accounts = governmentAccountService
        .getAll()
        .stream()
        .map(GovernmentAccountDto::fromGovernmentAccountWithoutPassword)
        .collect(Collectors.toList());
    } catch (Exception e) {
      e.printStackTrace();
      status = 500;
    }

    return Response
      .status(status)
      .entity(accounts)
      .build();
  }

  /**
   * Route to update a governmentAccount.
   *
   * @param governmentAccount
   * @return
   */
  @PUT
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response update(GovernmentAccount governmentAccount) {
    GovernmentAccountDto accountDto = null;
    int status = 200;

    try {
      accountDto = GovernmentAccountDto.fromGovernmentAccount(governmentAccountService.update(governmentAccount));
    } catch (Exception e) {
      status = 500;
    }

    return Response
      .status(status)
      .entity(accountDto)
      .build();
  }

  /**
   * Route to delete an existing governmentAccount.
   *
   * Will return successful response without content (204) on success.
   *
   * @param id    the id of the governmentAccount that needs to be deleted from the datebase.
   */
  @DELETE
  @Path("/{id}")
  public void delete(@PathParam("id") Long id) {
    governmentAccountService.delete(id);
  }
}

package rest;

import domain.RatePerKilometer;
import dto.RatePerKilometerDto;
import service.RatePerKilometerService;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/rates")
public class RatePerKilometerRest {
  @Inject
  private RatePerKilometerService ratePerKilometerService;

  /**
   * Route to get all ratesPerKilometer.
   *
   * @return  a response containing a list of ratesPerKilometer in JSON format on success
   */
  @GET
  @Produces({ APPLICATION_JSON })
  public Response getRatesPerKilometer() {
    List<RatePerKilometerDto> ratesPerKilometer = null;
    int status = 200;

    try {
      ratesPerKilometer = ratePerKilometerService
        .findAll()
        .stream()
        .map(RatePerKilometerDto::fromRatePerKilomter)
        .collect(Collectors.toList());
    } catch (Exception e) {
      status = 500;
    }

    return Response
      .status(status)
      .entity(ratesPerKilometer)
      .build();
  }

  /**
   * Route to create a ratePerKilometer.
   *
   * Required required body in JSON format:
   * {
   *    "id": "id",
   *    "rateCategory": "GASOLINE",
   *    "rateDate": "04/04/2018 00:00:00",
   *    "region": "Eindhoven"
   * }
   *
   * All above fields are required.
   *
   * @param ratePerKilometer  the ratePerKilometer to save in the database
   * @return                  a response containing the ratePerKilometer in JSON format on success
   */
  @POST
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response create(RatePerKilometer ratePerKilometer) {
    RatePerKilometerDto ratePerKilometerDto = null;
    int status = 200;

    try {
      ratePerKilometerDto = RatePerKilometerDto.fromRatePerKilomter(ratePerKilometerService.create(ratePerKilometer));
    } catch (Exception e) {
      if (e instanceof EJBException) {  // Caused by constraint
        status = 404;
      } else {
        status = 500;
      }
    }

    return Response
      .status(status)
      .entity(ratePerKilometerDto)
      .build();
  }

  /**
   * Route to update a ratePerKilometer.
   *
   * Required required body in JSON format:
   * {
   *    "id": "id",
   *    "rateCategory": "GASOLINE",
   *    "rateDate": "04/04/2018 00:00:00",
   *    "region": "Eindhoven"
   * }
   *
   * All above fields are required.
   *
   * @param ratePerKilometer  the ratePerKilometer to update in the database
   * @return                  a response containing the ratePerKilometer in JSON format on success
   */
  @PUT
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response update(RatePerKilometer ratePerKilometer) {
    RatePerKilometerDto ratePerKilometerDto = null;
    int status = 200;

    try {
      ratePerKilometerDto = RatePerKilometerDto.fromRatePerKilomter(ratePerKilometerService.update(ratePerKilometer));
    } catch (Exception e) {
      if (e instanceof EJBException) {  // Caused by constraint
        status = 404;
      } else {
        status = 500;
      }
    }

    return Response
      .status(status)
      .entity(ratePerKilometerDto)
      .build();
  }

  /**
   * Route to delete an existing ratePerKilometer.
   *
   * Required route params:
   *  - id
   *
   * @param id    the id of the ratePerKilometer that needs to be deleted from the database.
   * @return      a response containing status 204 (No Content) on success
   */
  @DELETE
  @Path("/{id}")
  public Response delete(@PathParam("id") Long id) {
    int status = 204;

    try {
      ratePerKilometerService.delete(id);
    } catch (Exception e) {
      status = 500;
    }

    return Response
      .status(status)
      .build();
  }
}

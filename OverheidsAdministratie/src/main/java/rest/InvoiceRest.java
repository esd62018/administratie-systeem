package rest;

import domain.Invoice;
import domain.RatePerKilometer;
import domain.TransLocation;
import domain.enums.PaymentStatus;
import domain.enums.RateCategory;
import dto.InvoiceDto;
import dto.JourneyDto;
import dto.VehicleDto;
import jms.MessageProducer;
import service.InvoiceService;
import service.RatePerKilometerService;
import utils.Config;
import utils.Constants;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/invoices")
public class InvoiceRest {
  private final String TRUSTED_API_TOKEN = Config.getInstance().getProperties().getProperty("api-token");
  private Client client;
  private List<InvoiceDto> foreignInvoicesToDelete = new ArrayList<>();

  @Inject
  private InvoiceService invoiceService;

  @Inject
  private RatePerKilometerService rateService;

  public InvoiceRest() {
    client = ClientBuilder.newClient();
  }

  /**
   * Route to get all invoices.
   *
   * @return  a response containing a list of invoices in JSON format on success
   */
  @GET
  @Produces({ APPLICATION_JSON })
  public Response getInvoices() {
    List<InvoiceDto> invoices = null;
    int status = 200;

    try {
      invoices = invoiceService
        .getInvoices()
        .stream()
        .map(InvoiceDto::fromInvoice)
        .collect(Collectors.toList());

        fillInvoices(invoices);
        for (InvoiceDto i : foreignInvoicesToDelete) {
          if (invoices.contains(i)) {
            invoices.remove(i);
          }
        }
        foreignInvoicesToDelete.clear();
    } catch (Exception e) {
      status = 500;
      e.printStackTrace();
    }

    return Response
      .status(status)
      .entity(invoices)
      .build();
  }

  /**
   * Route to get all the invoices from one owner
   *
   * Required router params:
   *  - ownerId
   *
   * Owner id should exist in the system
   *
   * @param ownerId     the id of the owner to find all the invoices for
   * @return            a response containing a list of invoices in JSON format on success
   */
  @GET
  @Path("/{ownerId}")
  @Produces({ APPLICATION_JSON })
  public Response getInvoicesFromOwner(@PathParam("ownerId") Long ownerId) {
    List<InvoiceDto> invoices = null;
    int status = 200;

    try {
      invoices = invoiceService
        .getInvoicesFromOwner(ownerId)
        .stream()
        .map(InvoiceDto::fromInvoice)
        .collect(Collectors.toList());

      fillInvoices(invoices);
    } catch (Exception e) {
      status = 500;
      e.printStackTrace();
    }

    return Response
      .status(status)
      .entity(invoices)
      .build();
  }

  /**
   * Route to create an invoice.
   *
   * Required required body in JSON format:
   * {
   *    "ownerId": "ownerId",
   *    "carTracker": "carTracker",
   *    "totalAmount": 100,
   *    "invoiceDate": "04/04/2018 00:00:00",
   *    "paymentStatus": "PAID"
   * }
   *
   * All above fields are required and ownerId should exist in the `RekeningRijdersApi`
   *
   * @param invoice     the invoice to save in the database
   * @return            a response containing the invoice in JSON format on success
   */
  @POST
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response create(Invoice invoice) {
    InvoiceDto invoiceDto = null;
    int status = 200;

    try {
      invoiceDto = InvoiceDto.fromInvoice(invoiceService.create(invoice));
    } catch (Exception e) {
      if (e instanceof EJBException) {  // Caused by constraint
        status = 404;
      } else {
        status = 500;
      }
    }

    return Response
      .status(status)
      .entity(invoiceDto)
      .build();
  }

  /**
   * Route to update an invoice.
   *
   * Required required body in JSON format:
   * {
   *    "id": 1,
   *    "ownerId": 1,
   *    "carTracker": "carTracker",
   *    "totalAmount": 100,
   *    "invoiceDate": "04/04/2018 00:00:00",
   *    "paymentStatus": "PAID"
   * }
   *
   * All above fields are required and ownerId should exist in the `RekeningRijdersApi`
   *
   * @param invoice     the invoice to update in the database
   * @return            a response containing the invoice in JSON format on success
   */
  @PUT
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response update(Invoice invoice) {
    InvoiceDto invoiceDto = null;
    int status = 200;

    try {
      invoiceDto = InvoiceDto.fromInvoice(invoiceService.update(invoice));
    } catch (Exception e) {
      status = 500;
    }

    return Response
      .status(status)
      .entity(invoiceDto)
      .build();
  }

  /**
   * Route to delete an existing invoice.
   *
   * Required route params:
   *  - id
   *
   * @param id    the id of the invoice that needs to be deleted from the database.
   * @return      a response containing status 204 (No Content) on success
   */
  @DELETE
  @Path("/{id}")
  public Response delete(@PathParam("id") Long id) {
    int status = 204;

    try {
      invoiceService.delete(id);
    } catch (Exception e) {
      status = 500;
    }

    return Response
      .status(status)
      .build();
  }

  /**
   * Route to generate invoices from last month.
   *
   * @return  a response containing a list of invoices in JSON format on success
   */
  @GET
  @Path("generate/{date}")
  @Produces({ APPLICATION_JSON })
  public Response generateInvoices(@PathParam("date") String date) {
    int status = 200;

    //Get vehicles
    List<VehicleDto> vehicles = new ArrayList<>();
    try {
      vehicles = client.target(Constants.DRIVERS_API_URL)
        .path("vehicles")
        .request(MediaType.APPLICATION_JSON)
        .header("token", TRUSTED_API_TOKEN)
        .get(new GenericType<List<VehicleDto>>(){});
    } catch (Exception e) {
      status = 500;
      e.printStackTrace();
    }

    // Create invoice for every vehicle
    List<InvoiceDto> invoices = new ArrayList<>();
    List<String> knownCarTrackers = new ArrayList<>();
    for (VehicleDto v : vehicles) {
      knownCarTrackers.add(v.getCarTracker()); // This will be used for foreign cars
      Invoice invoice = new Invoice();
      invoice.setCarTracker(v.getCarTracker());
      invoice.setInvoiceDate(date);
      invoice.setPaymentStatus(PaymentStatus.OPEN);

      // Add current owner id to invoice
      long ownerId;
      try {
        ownerId = client.target(Constants.DRIVERS_API_URL)
          .path("owners/by-car-tracker/" + invoice.getCarTracker())
          .request(MediaType.APPLICATION_JSON)
          .header("token", TRUSTED_API_TOKEN)
          .get(new GenericType<Long>(){});

        invoice.setOwnerId(ownerId);
      } catch (Exception e) {
        status = 500;
        e.printStackTrace();
      }

      // Add rate category to invoice
      String category;
      try {
        category = client.target(Constants.DRIVERS_API_URL)
          .path("vehicles/by-car-tracker/" + invoice.getCarTracker())
          .request(MediaType.APPLICATION_JSON)
          .header("token", TRUSTED_API_TOKEN)
          .get(new GenericType<String>(){});

        invoice.setCategory(category);
      } catch (Exception e) {
        status = 500;
        e.printStackTrace();
      }

      invoices.add(InvoiceDto.fromInvoice(invoice));
      invoiceService.create(invoice);
    }

    // Get all journeys
    List<JourneyDto> allJourneys = null;
    DateFormat formatBefore = new SimpleDateFormat("dd-MM-yyyy");
    DateFormat formatAfter = new SimpleDateFormat("yyyy-MM-dd");
    Date endDate = null;

    try {
      endDate = formatBefore.parse(date);
      GregorianCalendar cal = new GregorianCalendar();
      cal.setTime(endDate);
      cal.add(Calendar.MONTH, -1);
      Date startDate = cal.getTime();
      String startDateString = formatAfter.format(startDate);
      String endDateString = formatAfter.format(endDate);

      allJourneys = client.target(Constants.MOVEMENT_API_URL)
        .path("journeys/" + startDateString + "/" + endDateString)
        .request(MediaType.APPLICATION_JSON)
        .get(new GenericType<List<JourneyDto>>(){});
    } catch (Exception e) {
      e.printStackTrace();
    }

    // Add car to foreign car list of cartracker is not in knownCarTrackers or foreignCarTrackers
    List<String> foreignCarTrackers = new ArrayList<>();
    for (JourneyDto j : allJourneys) {
      if (!knownCarTrackers.contains(j.getSerialNumber()) && !foreignCarTrackers.contains(j.getSerialNumber())) {
        foreignCarTrackers.add(j.getSerialNumber());
      }
    }

    // Create invoice for every foreign cartracker
    List<InvoiceDto> foreignInvoices = new ArrayList<>();
    for (String f : foreignCarTrackers) {
      Invoice invoice = new Invoice();
      invoice.setCarTracker(f);
      invoice.setInvoiceDate(date);
      invoice.setPaymentStatus(PaymentStatus.OPEN);
      invoices.add(InvoiceDto.fromInvoice(invoice));
      foreignInvoices.add(InvoiceDto.fromInvoice(invoice));
      invoiceService.create(invoice);
    }

    fillInvoices(foreignInvoices);
    // TODO: foreignInvoice list should be sent with JMS

    return Response
      .status(status)
      .entity(invoices)
      .build();
  }

  private void fillInvoices(List<InvoiceDto> invoices) {
    ArrayList<RatePerKilometer> rates = new ArrayList<>(rateService.findAll());

    // Get all journeys per invoice
    for (InvoiceDto i : invoices) {
      String originCountry = "";
      double totalPrice = 0.0;
      List<JourneyDto> journeys = null;
      DateFormat formatBefore = new SimpleDateFormat("dd-MM-yyyy");
      DateFormat formatAfter = new SimpleDateFormat("yyyy-MM-dd");

      try {
      Date endDate = formatBefore.parse(i.getInvoiceDate());
      GregorianCalendar cal = new GregorianCalendar();
      cal.setTime(endDate);
      cal.add(Calendar.MONTH, -1);
      Date startDate = cal.getTime();
      String startDateString = formatAfter.format(startDate);
      String endDateString = formatAfter.format(endDate);
        journeys = client.target(Constants.MOVEMENT_API_URL)
          .path("journeys/" + i.getCarTracker() +  "/" + startDateString + "/" + endDateString)
          .request(MediaType.APPLICATION_JSON)
          .get(new GenericType<List<JourneyDto>>(){});
      } catch (Exception e) {
        e.printStackTrace();
      }

      // Get all translocations per journey
      for (JourneyDto j : journeys) {
        List<TransLocation> transLocations = null;
        try {
          transLocations = client.target(Constants.MOVEMENT_API_URL)
            .path("translocation/list/" + j.getId())
            .request(MediaType.APPLICATION_JSON)
            .get(new GenericType<List<TransLocation>>(){});

          // Get rate and price per translocation
          RatePerKilometer rate = null;
          TransLocation previousTranslocation = null;
          List<TransLocation> transLocationsToRemove = new ArrayList<>();
          for (TransLocation t : transLocations) {
                if(originCountry.equals("")){
                  originCountry = t.getCountryCode();
                }
                String region = null;
                if (t.getLon() > 2.6 && t.getLon() < 6.3) {
                  if (t.getLat() > 49.5 && t.getLat() < 51.4) {
                    if (t.getLat() >= 50.75) {
                      region = "Vlaanderen";
                    } else {
                      region = "Wallonië";
                    }
                  }
                }

                // Check if region is in Belgium and should invoice
                if (region != null) {
                  for (RatePerKilometer r : rates) {
                    if (r.getRegion().equals(region)) {
                      if (i.getCategory() == null) {
                        i.setCategory(RateCategory.DIESEL.toString());
                      }
                      if (r.getRateCategory().toString().toUpperCase().equals(i.getCategory().toUpperCase())) {
                        Date rateStartDate = formatBefore.parse(r.getStartDate());
                        Date rateEndDate = formatBefore.parse(r.getEndDate());
                        if (t.getDateTime().before(rateEndDate) && t.getDateTime().after(rateStartDate)) {
                          rate = r;
                        }
                      }
                    }
                  }

                  if (previousTranslocation != null) {
                    t.setPreviousLat(previousTranslocation.getLat());
                    t.setPreviousLon(previousTranslocation.getLon());
                    t.setDistance(distance(t.getPreviousLat(), t.getLat(), t.getPreviousLon(), t.getLon(), 0.0, 0.0));
                    t.setPrice(t.getDistance() * rate.getRatePrice());
                    totalPrice = totalPrice + t.getPrice();
                  }
                } else {
                  transLocationsToRemove.add(t);
                }

            previousTranslocation = t;
          }

          for (TransLocation t : transLocationsToRemove) {
            if (transLocations.contains(t)) {
              transLocations.remove(t);
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        }

        j.setTransLocations(transLocations);
      }

      i.setJourneys(journeys);
      i.setTotalAmount(round(totalPrice, 2));
      if (i.getTotalAmount() <= 0) {
        i.setPaymentStatus(PaymentStatus.PAID);
      }

      if (i.getOwnerId() == 0) {
        if (i.getJourneys().size() > 0) {
          int totalTranslocations = 0;
          for (JourneyDto j : i.getJourneys()) {
            if (j.getTransLocations().size() > 1) {
              totalTranslocations = totalTranslocations + j.getTransLocations().size();
            }
          }

          if (totalTranslocations == 0) {
            foreignInvoicesToDelete.add(i);
          }
        } else {
          foreignInvoicesToDelete.add(i);
          break;
        }
      }
      MessageProducer.sendNewInvoice(i, originCountry);
    }
  }

  /**
   * Calculate distance between two points in latitude and longitude taking
   * into account height difference. If you are not interested in height
   * difference pass 0.0. Uses Haversine method as its base.
   *
   * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters
   * el2 End altitude in meters
   * @returns Distance in Kilometers
   */
  public static double distance(double lat1, double lat2, double lon1,
                                double lon2, double el1, double el2) {

    final int R = 6371; // Radius of the earth

    double latDistance = Math.toRadians(lat2 - lat1);
    double lonDistance = Math.toRadians(lon2 - lon1);
    double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
      + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
      * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    double distance = R * c * 1000; // convert to meters

    double height = el1 - el2;

    distance = Math.pow(distance, 2) + Math.pow(height, 2);

    return Math.sqrt(distance) / 1000;
  }

  public static double round(double value, int places) {
    if (places < 0) throw new IllegalArgumentException();

    BigDecimal bd = new BigDecimal(value);
    bd = bd.setScale(places, RoundingMode.HALF_UP);
    return bd.doubleValue();
  }
}

package rest;

import domain.GovernmentAccount;
import dto.GovernmentAccountDto;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import service.GovernmentAccountService;
import utils.Constants;

import javax.ejb.EJBException;
import javax.ejb.EJBTransactionRolledbackException;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/auth")
public class AuthRest {
  private static final String DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";
  private static final DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

  @Inject
  private GovernmentAccountService governmentAccountService;

  public AuthRest() {

  }

  /**
   * Route to authenticate governmentAccount with login credentials.
   *
   * @param governmentAccount
   * @return
   */
  @POST
  @Path("/login")
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response authenticate(GovernmentAccount governmentAccount) {
    ResponseBody responseBody = new ResponseBody();
    int status = 200;

    try {
      GovernmentAccountDto accountDto = GovernmentAccountDto.fromGovernmentAccount(governmentAccountService.authenticate(governmentAccount));
      LocalDateTime localDateTime = new Date()
        .toInstant()
        .atZone(ZoneId.systemDefault())
        .toLocalDateTime()
        .plusDays(3);

      Date expiration = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

      String token = Jwts.builder()
        .setExpiration(expiration)
        .setSubject(String.valueOf(accountDto.getId()))
        .signWith(SignatureAlgorithm.HS512, Constants.JWS_SECRET)
        .compact();

      responseBody.setAuth(accountDto);
      responseBody.setExpireDate(dateFormat.format(expiration));
      responseBody.setToken(token);

    } catch (Exception e) {
      if (e instanceof EJBTransactionRolledbackException) {   // Caused by no account found
        status = 404;
      } else {
        status = 500;
      }
    }

    return Response
      .status(status)
      .entity(responseBody)
      .build();
  }

  /**
   * Route to create a governmentAccount.
   *
   * @param governmentAccount
   * @return
   */
  @POST
  @Path("/sign-up")
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response signUp(GovernmentAccount governmentAccount) {
    GovernmentAccountDto accountDto = null;
    int status = 200;

    try {
      accountDto = GovernmentAccountDto.fromGovernmentAccount(governmentAccountService.signUp(governmentAccount));
    } catch (Exception e) {
      if (e instanceof EJBException) {  // Caused by constraint
        status = 404;
      } else {
        status = 500;
      }
    }

    return Response
      .status(status)
      .entity(accountDto)
      .build();
  }

  public class ResponseBody {
    private GovernmentAccountDto auth;
    private String token;
    private String expireDate;

    public ResponseBody() {

    }

    public GovernmentAccountDto getAuth() {
      return auth;
    }

    public void setAuth(GovernmentAccountDto auth) {
      this.auth = auth;
    }

    public String getToken() {
      return token;
    }

    public void setToken(String token) {
      this.token = token;
    }

    public String getExpireDate() {
      return expireDate;
    }

    public void setExpireDate(String expireDate) {
      this.expireDate = expireDate;
    }
  }
}

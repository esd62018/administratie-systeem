package rest;

import dto.VehicleHistoryDto;
import utils.Config;
import utils.Constants;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/vehicle-histories")
public class VehicleHistoryRest {
  private final String TRUSTED_API_TOKEN = Config.getInstance().getProperties().getProperty("api-token");
  private Client client;

  public VehicleHistoryRest() {
    client = ClientBuilder.newClient();
  }

  @GET
  @Produces({ APPLICATION_JSON })
  public Response getAllVehicleHistories() {
    List<VehicleHistoryDto> vehicleHistories = null;
    int status = 200;

    try {
      vehicleHistories = client.target(Constants.DRIVERS_API_URL)
        .path("vehicle-histories")
        .request(MediaType.APPLICATION_JSON)
        .header("token", TRUSTED_API_TOKEN)
        .get(new GenericType<List<VehicleHistoryDto>>(){});
    } catch (Exception e) {
      status = 500;
      e.printStackTrace();
    }

    return Response
      .status(status)
      .entity(vehicleHistories)
      .build();
  }
}

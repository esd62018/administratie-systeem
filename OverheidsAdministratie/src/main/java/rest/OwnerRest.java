package rest;

import dto.OwnerDto;
import utils.Config;
import utils.Constants;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/owners")
public class OwnerRest {
  private final String TRUSTED_API_TOKEN = Config.getInstance().getProperties().getProperty("api-token");

  private Client client;

  public OwnerRest() {
    client = ClientBuilder.newClient();
  }

  @GET
  @Produces({ APPLICATION_JSON })
  public Response getAllVehicleHistories() {
    List<OwnerDto> owners = null;
    int status = 200;

    try {
      owners = client.target(Constants.DRIVERS_API_URL)
        .path("owners")
        .request(MediaType.APPLICATION_JSON)
        .header("token", TRUSTED_API_TOKEN)
        .get(new GenericType<List<OwnerDto>>(){});
    } catch (Exception e) {
      status = 500;
      e.printStackTrace();
    }

    return Response
      .status(status)
      .entity(owners)
      .build();
  }
}

package utils;

public class Constants {
  public static final String GOVERNMENT_API_URL = "http://localhost:56938/OverheidsAdministratie/api";
//  public static final String DRIVERS_API_URL = "http://localhost:5252/RekeningRijders/api";
  //public static final String DRIVERS_API_URL = "http://localhost:8080/RekeningRijders/api";
public static final String DRIVERS_API_URL = "http://localhost:18953/RekeningRijders/api";
  public static final String MOVEMENT_API_URL = "http://localhost:16322/VerplaatsingRegistratie/api";

  public static final String JWS_SECRET = "SecretKey333";
}
